import org.junit.Test;
import isarescu.mihai.lab3.ex1.Robot;

import static org.junit.Assert.assertEquals;

public class TestRobot {

    @Test
    public void testRobots(){
        Robot r1 = new Robot();
        assertEquals("Testam constructorul",1,r1.x);
        r1.change(10);
        assertEquals("Testam metoda change",11,r1.x);
    }
}
