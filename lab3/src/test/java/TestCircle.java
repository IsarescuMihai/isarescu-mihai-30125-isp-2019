import org.junit.Test;
import isarescu.mihai.lab3.ex2.Circle;

import static org.junit.Assert.assertEquals;

public class TestCircle {

    @Test
    public void testCircles(){
        Circle c1 = new Circle(1);
        assertEquals(6.28,c1.getArea(),0.005);
        Circle c2 = new Circle(1,"blue");
        assertEquals(1.0,c2.getRadius(),0.005);
    }
}
