import org.junit.Test;
import isarescu.mihai.lab3.ex3.Author;

import static org.junit.Assert.assertEquals;
public class TestAuthor {

    @Test
    public void testGeeks(){
        Author a1 = new Author("Marcel","Marcel@mail.com",'m');
        assertEquals("Marcel",a1.getName());
        assertEquals("Marcel@mail.com",a1.getEmail());
        assertEquals('m',a1.getGender());
        a1.setEmail("newmail@mail.com");
        assertEquals("newmail@mail.com",a1.getEmail());
    }
}
