import org.junit.Test;
import isarescu.mihai.lab3.ex4.MyPoint;

import static org.junit.Assert.assertEquals;

public class TestMyPoint {
    @Test
    public void testPoints(){
        MyPoint p1 = new MyPoint(1,1);
        MyPoint p2 = new MyPoint(1,2);
        assertEquals(1.41,p1.distance(0,0),0.01);
        assertEquals(1,p1.distance(p2),0.01);
    }
}
