import org.junit.Test;
import isarescu.mihai.lab3.ex5.Flower;

import static org.junit.Assert.assertEquals;

public class TestFlower {
    @Test
    public void testFlowers(){
        Flower f1 = new Flower(1);
        Flower f2 = new Flower(3);
        Flower f3 = new Flower(5);

        assertEquals("numaram flori",3,Flower.getNumOfFlowers());
    }
}
