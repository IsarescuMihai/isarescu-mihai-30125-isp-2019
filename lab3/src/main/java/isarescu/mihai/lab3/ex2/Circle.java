package isarescu.mihai.lab3.ex2;

public class Circle {
    private double radius = 1.0f;
    private String color = "red";

    public Circle(double radius){
        this.radius = radius;
    }

    public Circle(double radius,String color){
        this.radius = radius;
        this.color = color;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return Math.acos(-1)*2*radius;//Math.acos(-1) = pi
    }

    public static void main(String[] args) {
        System.out.println("dummy main");
    }
}
