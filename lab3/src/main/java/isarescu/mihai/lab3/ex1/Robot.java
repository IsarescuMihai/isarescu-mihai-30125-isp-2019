package isarescu.mihai.lab3.ex1;

public class Robot {
    public int x;
    public Robot(){
        this.x = 1;
    }
    public void change(int amount){
        this.x += amount;
    }

    @Override
    public String toString() {
        return Integer.toString(x);
    }

    public static void main(String[] args) {
        System.out.println("dummy main");
    }
}
