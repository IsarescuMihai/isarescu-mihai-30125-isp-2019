package isarescu.mihai.lab3.ex5;

public class Flower{
    int petal;
    private static int numOfFlowers;
    public Flower(int p){
        numOfFlowers++;
        petal=p;
        System.out.println("New flower has been created!");
    }

    public static int getNumOfFlowers(){
        return numOfFlowers;
    }

    public static void main(String[] args) {
        System.out.println("dummy main");
    }
}