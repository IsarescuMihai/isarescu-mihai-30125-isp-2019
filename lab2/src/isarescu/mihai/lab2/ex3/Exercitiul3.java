package isarescu.mihai.lab2.ex3;
import java.util.Scanner;

public class Exercitiul3 {

    public static boolean IsPrime(int x)
    {
        if(x <= 1)
            return false;
        boolean k = true;
        for(int d = 2; d <= Math.sqrt((double)x); d++){
            if(x%d == 0)
                k = false;
        }
        return k;
    }
    public static void PrintPrimeNumbers(int a, int b){
        System.out.println("The prime numbers between " + a + " and " + b +" are:");
        for(int i = a; i<= b; i++){
            if(IsPrime(i))
                System.out.print(i+" ");
        }
    }

    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter a and b");
        int a = input.nextInt();
        int b = input.nextInt();

        PrintPrimeNumbers(a,b);
    }
}
