package isarescu.mihai.lab2.ex7;
import java.util.Scanner;

public class Exercitiul7 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);

        //generate numbers between 0 and 10
        int target = (int)(10*Math.random());

        int numOfTries = 0;

        while(numOfTries < 3){
            System.out.println("Guess the number, you have " + (3-numOfTries) + " tries left");

            int triedNumber = input.nextInt();
            if(triedNumber == target) {
                System.out.println("Congratz, you WON!");
                break;
            }
            else if(triedNumber > target){
                System.out.println("The number is smaller");
                numOfTries++;
            }
            else/*if(triedNumber < target*/{
                System.out.println("The number is bigger");
                numOfTries++;
            }
        }

        if(numOfTries == 3)
            System.out.println("You LOST");
    }
}
