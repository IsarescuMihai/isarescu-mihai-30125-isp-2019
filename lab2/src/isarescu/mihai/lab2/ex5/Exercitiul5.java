package isarescu.mihai.lab2.ex5;
import java.util.Scanner;

public class Exercitiul5 {

    private static int[] GenerateRandomArray(int sizeOfArray){
        int[] array = new int[sizeOfArray];
        for(int i = 0; i < array.length; i++){
            array[i] = (int)(100*Math.random());
        }
        return array;
    }

    private static int[] BubbleSort(int[] array){
        for(int i = 0; i < array.length-1; i++)
            for(int j = i+1; j < array.length; j++)
                if(array[i] > array[j]){
                    int aux = array[i];
                    array[i] = array[j];
                    array[j] = aux;
                }

        return array;
    }

    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.println("Introduce the size of the array");

        int n = input.nextInt();

        int[] a = GenerateRandomArray(n);

        System.out.println("The random array is: ");
        for(int i = 0; i < a.length; i++)
            System.out.print(a[i] + " ");

        a = BubbleSort(a);

        System.out.println("The sorted array is: ");
        for(int i = 0; i < a.length; i++)
            System.out.print(a[i] + " ");

    }
}
