package isarescu.mihai.lab2.ex6;
import java.util.Scanner;

public class Exercitiul6 {

    private static int IterativeFactorial(int n){
        if(n == 0 || n == 1){
            return 1;
        }
        else if (n >= 2){
            int factorial = 1;
            for(int number = 2; number <= n; number++){
                factorial *= number;
            }
            return factorial;
        }
        //returnam -1 daca introducem un numar negativ
        else return -1;
    }

    private static int RecursiveFactorial(int n){
        //returnam -1 daca introducem un numar negativ
        if(n < 0)
            return -1;
        else{
            if(n == 0)
                return 1;
            if(n == 1)
                return 1;
            return n*RecursiveFactorial(n-1);
        }
    }

    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the number n");

        int n = input.nextInt();
        System.out.println("This is the iterative factorial: " + IterativeFactorial(n));
        System.out.println("This is the recursive factorial: " + RecursiveFactorial(n));
    }
}
