package isarescu.mihai.lab2.ex4;
import java.util.Scanner;

public class Exercitiul4 {

    private static int[] GenerateRandomArray(int sizeOfArray){
        int[] array = new int[sizeOfArray];
        for(int i = 0; i < array.length; i++){
            array[i] = (int)(100*Math.random());
        }
        return array;
    }

    public static void main(String[] args){
        System.out.println("Enter the size of the random array:");

        Scanner input = new Scanner(System.in);

        int n = input.nextInt();
        int[] a = GenerateRandomArray(n);
        int max = Integer.MIN_VALUE;
        for(int i = 0; i < a.length; i++)
            if(a[i] > max)
                max = a[i];

        System.out.println("Random array is:");
        for(int i = 0; i < a.length; i++)
            System.out.print(a[i] + " ");
        System.out.println("\nThe maximum of the array is: " + max);
    }
}
