package isarescu.mihai.lab2.ex1;
import java.util.Scanner;
import java.math.*;

public class Exercitiul1 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);

        System.out.println("Enter 2 numbers:");
        int number1 = input.nextInt();
        int number2 = input.nextInt();

        System.out.println("The maximum of the two is: " + Math.max(number1,number2));
    }
}
