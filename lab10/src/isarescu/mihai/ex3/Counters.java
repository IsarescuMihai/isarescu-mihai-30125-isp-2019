package isarescu.mihai.ex3;

public class Counters {

    public static void main(String[] args) {
        Counter c1 = new Counter(0,100,"1");
        Counter c2 = new Counter(100,200,"2");


        c1.run();
        c2.run();
    }
}

class Counter extends Thread{
    private int start;
    private int finish;
    public Counter(int start, int finish, String name){
        super(name);
        this.start = start;
        this.finish = finish;
    }
    @Override
    public void run(){
        for(int i = start; i <= finish; i++){
            System.out.println("counter" + this.getName() + " at " + i);
        }
        System.out.println("end counter" + this.getName());
    }
}
