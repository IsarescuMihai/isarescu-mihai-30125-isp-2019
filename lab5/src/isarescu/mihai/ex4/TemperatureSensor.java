package isarescu.mihai.ex4;

public class TemperatureSensor extends Sensor {
    TemperatureSensor(String location){
        super(location);
    }

    @Override
    public int readValue(){
        return (int)(100*Math.random());
    }
}
