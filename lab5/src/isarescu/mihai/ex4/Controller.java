package isarescu.mihai.ex4;

public class Controller {
    private LightSensor ls;
    private TemperatureSensor ts;
    private static volatile Controller instance;
    private Controller(){
        ls = new LightSensor("geam");
        ts = new TemperatureSensor("baie");
    }

    public static Controller getInstance()
    {
        synchronized (Controller.class){
            if(instance == null)
                instance = new Controller();
        }
        return instance;
    }

    public String control(){
        int lsValue;
        int tsValue;
        if(ls.getLocation().equals("-1"))
            lsValue = -1;
        else
            lsValue = ls.readValue();
        if(ts.getLocation().equals("-1"))
            tsValue = -1;
        else
            tsValue = ts.readValue();
        return "Light sensor value: " + lsValue + "\n"
                + "Temperature sensor value: " + tsValue;
    }
}
