package isarescu.mihai.ex4;

public class LightSensor extends Sensor {
    LightSensor(String location){
        super(location);
    }

    @Override
    public int readValue(){
        return (int)(100*Math.random());
    }
}
