package isarescu.mihai.ex3;

public abstract class Sensor {
    private String location;

    public abstract int readValue();

    public String getLocation(){
        return location;
    }

    public Sensor(String location){
        this.location = location;
    }
}
