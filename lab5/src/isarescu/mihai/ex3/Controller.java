package isarescu.mihai.ex3;

public class Controller {
    private LightSensor ls;
    private TemperatureSensor ts;

    Controller(LightSensor ls, TemperatureSensor ts) {
        if (ls != null)
            this.ls = ls;
        else
            this.ls = new LightSensor("-1");
        if (ts != null)
            this.ts = ts;
        else
            this.ts = new TemperatureSensor("-1");
    }

    public String control() {
        int lsValue;
        int tsValue;
        if (ls.getLocation().equals("-1"))
            lsValue = -1;
        else
            lsValue = ls.readValue();
        if (ts.getLocation().equals("-1"))
            tsValue = -1;
        else
            tsValue = ts.readValue();
        return "Light sensor value: " + lsValue + "\n"
                + "Temperature sensor value: " + tsValue;
    }
}
