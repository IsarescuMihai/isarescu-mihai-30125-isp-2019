package isarescu.mihai.ex3;

import java.lang.Thread;

public class TestClass {
    public static void main(String[] args) throws InterruptedException{
        LightSensor lightSensor = new LightSensor("geam");
        TemperatureSensor temperatureSensor = new TemperatureSensor("baie");

        Controller c1 = new Controller(lightSensor,temperatureSensor);

        for(int i = 0; i < 20; i++){
            System.out.println(c1.control());
            Thread.sleep(1000);
        }

    }
}
