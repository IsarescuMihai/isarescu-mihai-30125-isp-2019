package isarescu.mihai.ex3;

public class LightSensor extends Sensor{
    LightSensor(String location){
        super(location);
    }

    @Override
    public int readValue(){
        return (int)(100*Math.random());
    }
}
