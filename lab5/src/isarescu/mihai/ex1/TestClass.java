package isarescu.mihai.ex1;

public class TestClass {
    public static void main(String[] args) {
        Shape[] shapes = new Shape[3];
        shapes[0] = new Circle(4.5,"blue",true);
        shapes[1] = new Rectangle(3,4,"green",false);
        shapes[2] = new Square(4,"yellow",true);

        for(int i = 0; i < shapes.length; i++){
            System.out.println(shapes[i].toString());
        }
    }
}
