package isarescu.mihai.ex2;

public class ProxyImage implements Image {
    private RealImage realImage;
    private RotatedImage rotatedImage;
    private String fileName;
    private boolean rotated;

    public ProxyImage(String fileName, boolean rotated){
        this.fileName = fileName;
        this.rotated = rotated;
    }

    @Override
    public void display() {
        if(rotated) {
            if(rotatedImage == null){
                rotatedImage = new RotatedImage(fileName);
            }
            rotatedImage.display();
        }
        else{
            if(realImage == null){
                realImage = new RealImage(fileName);
            }
            realImage.display();
        }
    }
}
