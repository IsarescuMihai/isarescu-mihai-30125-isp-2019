package isarescu.mihai.ex2;

public class TestClass {
    public static void main(String[] args) {
        ProxyImage pi1 = new ProxyImage("proxy1.png",false);
        ProxyImage pi2 = new ProxyImage("proxy2.jpeg",true);

        pi1.display();
        pi2.display();
    }
}
