package isarescu.mihai.ex1;

public class CofeeDrinker{
    void drinkCofee(Cofee c) throws TemperatureException, ConcentrationException, CofeeNumberException{
        if(c.getTemp()>60)
            throw new TemperatureException(c.getTemp(),"Cofee is to hot!");
        if(c.getConc()>50)
            throw new ConcentrationException(c.getConc(),"Cofee concentration to high!");
        //if(c.numOfCofee > c.MAX_NUM_OF_COFFEE)
        //    throw new CofeeNumberException(c.numOfCofee,"Too much cofee made!");
        System.out.println("Drink cofee:"+c);
    }
}//.class