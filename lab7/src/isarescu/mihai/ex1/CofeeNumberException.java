package isarescu.mihai.ex1;

public class CofeeNumberException extends Exception {
    int num;
    public CofeeNumberException(int num,String msg) {
        super(msg);
        this.num = num;
    }

    int getNum(){
        return num;
    }
}
