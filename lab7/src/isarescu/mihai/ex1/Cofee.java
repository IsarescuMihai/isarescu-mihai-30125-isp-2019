package isarescu.mihai.ex1;


public class Cofee{
    public static final int MAX_NUM_OF_COFFEE = 3;
    private int temp;
    private int conc;
    public static int numOfCofee = 0;

    Cofee(int t,int c)throws CofeeNumberException{
        temp = t;
        conc = c;
        numOfCofee++;
        if(numOfCofee > MAX_NUM_OF_COFFEE)
            throw new CofeeNumberException(numOfCofee,"Too much cofee made");
    }
    int getTemp(){return temp;}
    int getConc(){return conc;}
    public String toString(){return "[cofee temperature="+temp+":concentration="+conc+"]";}
}//.class