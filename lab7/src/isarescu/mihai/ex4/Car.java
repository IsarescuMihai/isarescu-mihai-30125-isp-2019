package isarescu.mihai.ex4;

import java.io.Serializable;
import java.util.Objects;

public class Car implements Serializable {
    private float price;
    private String model;

    public Car(float price, String model) {
        this.price = price;
        this.model = model;
    }

    public Car() {
    }

    public Car(float price) {
        this.price = price;
    }

    public Car(String model) {
        this.model = model;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public String toString() {
        return "Car{" +
                "price=" + price +
                ", model='" + model + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return model.equals(car.model);
    }

    @Override
    public int hashCode() {
        return Objects.hash(model);
    }
}
