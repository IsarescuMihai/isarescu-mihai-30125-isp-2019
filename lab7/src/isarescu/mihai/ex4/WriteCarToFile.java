package isarescu.mihai.ex4;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Scanner;

public class WriteCarToFile {
    private static final String filePath = "src\\isarescu\\mihai\\ex4\\carFile.txt";

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        ArrayList<Car> masini = new ArrayList<>();

        System.out.print("how many cars are you interested in: ");

        int numberOfCars = input.nextInt();

        //citim masinile de la user
        for(int i = 0; i < numberOfCars; i++)
            masini.add(createCar());

        //salvam masinile in fisier
        addObjectsToFile(masini);

        ArrayList<Car> masiniDinFisier = readCarsFromFile();
        System.out.println("masinile salvate in fisier: ");
        for(Car masina : masiniDinFisier)
            System.out.println(masina);
    }

    public static Car createCar(){
        String model;
        float price;
        System.out.print("what is the car model: ");
        Scanner input = new Scanner(System.in);

        model = input.nextLine();

        System.out.print("how much does it cost: ");

        price = input.nextFloat();

        return new Car(price,model);
    }

    public static void addObjectsToFile(ArrayList<Car> masini){
        try{
            //write objects to the file
            FileOutputStream fileOut = new FileOutputStream(filePath);
            ObjectOutputStream  objectOut = new ObjectOutputStream(fileOut);
            for(Car masina : masini)
                objectOut.writeObject(masina);

            objectOut.close();
            fileOut.close();
        }

        catch (Exception e){
            System.out.println(e.getMessage());
        }


    }

    public static ArrayList<Car> readCarsFromFile(){
        //the return array
        ArrayList<Car> masiniCitite = new ArrayList<>();

        try {
            //read objects from the file
            FileInputStream fileIn = new FileInputStream(filePath);
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);

            Car masina;
            while((masina = (Car)objectIn.readObject()) != null)
                masiniCitite.add(masina);
            objectIn.close();
            fileIn.close();
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }


        return masiniCitite;
    }
}
