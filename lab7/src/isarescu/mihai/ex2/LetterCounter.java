package isarescu.mihai.ex2;

import java.io.*;
import java.util.*;

public class LetterCounter {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        try {
            //BufferedReader inputFile = new BufferedReader(new FileReader("D:\\Work\\Important work\\POLI STUFF\\ISP\\isarescu-mihai-30125-isp-2019\\lab7\\src\\isarescu\\mihai\\ex2\\data.txt"));
            BufferedReader inputFile = new BufferedReader(new FileReader("src\\isarescu\\mihai\\ex2\\data.txt"));
            String s, s2 = new String();
            while((s = inputFile.readLine())!= null)
                s2 += s + "\n";
            inputFile.close();

            String caracter;
            System.out.print("enter the character you want to count: ");
            caracter = input.nextLine();
            char c = caracter.charAt(0);

            int charCounter = 0;
            for(int i = 0; i < s2.length();i++){
                if(c == s2.charAt(i))
                    charCounter++;
            }

            System.out.println("the source file contains the character " + c + " " + charCounter + " time(s)");

        }catch (Exception e){
            System.out.println("Exception: " + e.getMessage());
        }

    }
}
