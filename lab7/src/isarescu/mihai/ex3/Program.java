package isarescu.mihai.ex3;

import java.io.*;

public class Program {
    public static void main(String[] args) throws IOException{
        String filePath = "src\\isarescu\\mihai\\ex3\\" + args[0];

        BufferedReader inputFile = new BufferedReader(new FileReader(filePath));

        String line,text = new String();
        while((line = inputFile.readLine()) != null){
            text += (line+"\n");
        }

        if(args[1].equals("encrypt")) {
            //encrypted file
            try {
                String outputText = encryptMessage(text);

                PrintWriter outputFile = new PrintWriter(
                        new BufferedWriter(new FileWriter("src\\isarescu\\mihai\\ex3\\out.enc")));
                outputFile.println(outputText);
                outputFile.close();
            } catch (EOFException e) {
                System.err.println("End of stream");
            }
        }

        else if(args[1].equals("decrypt")) {
            //decrypted file
            try {
                String outputText = decryptMessage(text);

                PrintWriter outputFile = new PrintWriter(
                        new BufferedWriter(new FileWriter("src\\isarescu\\mihai\\ex3\\out.dec")));
                outputFile.println(outputText);
                outputFile.close();
            } catch (EOFException e) {
                System.err.println("End of stream");
            }
        }
        else System.out.println("the second argument is wrong");
    }

    public static String encryptMessage(String msg){
        char[] newMessage = msg.toCharArray();
        for(int i = 0;i < msg.length(); i++)
            newMessage[i]++;

        return new String(newMessage);
    }

    public static String decryptMessage(String msg){
        char[] newMessage = msg.toCharArray();
        for(int i = 0;i < msg.length(); i++)
            newMessage[i]--;

        return new String(newMessage);
    }
}
