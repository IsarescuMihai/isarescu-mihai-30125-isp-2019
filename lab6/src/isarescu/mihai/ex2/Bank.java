package isarescu.mihai.ex2;
import java.util.*;

public class Bank {

    private ArrayList<BankAccount> accounts;

    public Bank(ArrayList<BankAccount> accounts) {
        this.accounts = accounts;
    }

    public void addAccount(String owner, double balance){
        accounts.add(new BankAccount(owner,balance));
    }

    public void printAccounts(){

        Collections.sort(accounts);
        for(BankAccount acc : accounts){
            System.out.println(acc.toString());
        }
    }

    public ArrayList<BankAccount> getAllAccounts(){
        return accounts;
    }

    public void printAccounts(double minBalance, double maxBalance){
        Collections.sort(accounts);
        ArrayList<BankAccount> goodAccounts = new ArrayList<BankAccount>();
        for(BankAccount acc : accounts){
            if(acc.getBalance()>=minBalance && acc.getBalance() <=maxBalance)
                goodAccounts.add(acc);
        }
        for(BankAccount acc: goodAccounts)
            System.out.println(acc.toString());
    }

    public BankAccount getAccount(String owner){
        int index = accounts.indexOf(new BankAccount(owner,0));
        return accounts.get(index);
    }
}
