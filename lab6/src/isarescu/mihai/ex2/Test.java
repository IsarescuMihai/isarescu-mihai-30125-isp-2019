package isarescu.mihai.ex2;

import java.util.*;

public class Test {
    public static void main(String[] args) {
        ArrayList<BankAccount> array = new ArrayList<BankAccount>();
        Bank b = new Bank(array);

        b.addAccount("Mihai",100);
        b.addAccount("Alex",200);
        b.addAccount("Mihail",300);
        b.addAccount("Maria",150);
        b.addAccount("Andreea",225);

        BankAccount Mihai = b.getAccount("Maria");
        System.out.println(Mihai);

        ArrayList<BankAccount> allAccounts = b.getAllAccounts();

        System.out.println("All accounts ordered by balance");
        b.printAccounts();

        System.out.println("All acccounts from 150 to 250 balance");
        b.printAccounts(150,250);

        System.out.println("Unordered bank accounts");
        for(BankAccount acc : allAccounts)
            System.out.println(acc.toString());

        Collections.sort(allAccounts,new SortByName());

        System.out.println("Bank accounts ordered by name");
        for(BankAccount acc : allAccounts)
            System.out.println(acc.toString());
    }
}

class SortByName implements Comparator<BankAccount>{
    @Override
    public int compare(BankAccount ba1, BankAccount ba2){
        return ba1.getOwner().compareToIgnoreCase(ba2.getOwner());
    }
}