package isarescu.mihai.ex4;

import java.util.*;

public class Dictionary {

    private HashMap<Word,Definition> dictionary;

    public Dictionary(HashMap<Word, Definition> dictionary) {
        this.dictionary = dictionary;
    }

    public void addWord (Word w, Definition d){
        dictionary.put(w,d);
    }

    public Definition getDefinition(Word w){
        return dictionary.get(w);
    }

    public Set<Word> getAllWords(){
        return dictionary.keySet();
    }

    public Collection<Definition> getAllDefinitions(){
        return dictionary.values();
    }
}

class Word {
    private String name;

    public Word(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Word word = (Word) o;
        return name.equals(word.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Word{" +
                "name='" + name + '\'' +
                '}';
    }
}

class Definition {
    private String description;

    public Definition(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Definition{" +
                "description='" + description + '\'' +
                '}';
    }
}