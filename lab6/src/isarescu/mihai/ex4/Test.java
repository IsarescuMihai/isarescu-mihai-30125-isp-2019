package isarescu.mihai.ex4;
import java.util.*;

public class Test {
    public static void main(String[] args) {
        HashMap<Word,Definition> dic = new HashMap<>();
        Dictionary d = new Dictionary(dic);

        Word w1 = new Word("cuvantul");
        Definition d1 = new Definition("minunat");
        d.addWord(w1,d1);

        Word w2 = new Word("proful");
        Definition d2 = new Definition("micut");
        d.addWord(w2,d2);

        Word w3 = new Word("povesti");
        Definition d3 = new Definition("bulgaresti");
        d.addWord(w3,d3);

        Word w4 = new Word("cantece");
        Definition d4 = new Definition("de leagan");
        d.addWord(w4,d4);

        Word w5 = new Word("nani");
        Definition d5 = new Definition("somnic");
        d.addWord(w5,d5);

        System.out.println("toate cuvintele");
        Set<Word> toateCuvintele = d.getAllWords();
        for(Word cuvintel : toateCuvintele){
            System.out.println(cuvintel);
        }

        System.out.println("toate definitiile");
        Collection<Definition> toateDefinitiile = d.getAllDefinitions();
        for(Definition yum : toateDefinitiile){
            System.out.println(yum);
        }

        Definition def;
        Word cuv = new Word("povesti");
        def = d.getDefinition(cuv);
        System.out.println("definitia cuvantului "+ cuv + " este " + def);
    }
}
