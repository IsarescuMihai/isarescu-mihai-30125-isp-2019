package isarescu.mihai.ex1;

public class Test {
    public static void main(String[] args) {
        BankAccount Mihai = new BankAccount("Mihai",100);
        BankAccount Alex = new BankAccount("Alex",200);
        BankAccount Mihai2 = new BankAccount("Mihai",300);

        if(Mihai.equals(Mihai2))
            System.out.println("Mihai are 2 conturi");

        if(Mihai.equals(Alex))
            System.out.println("Pe Mihai il mai cheama si Alex");
    }

}
