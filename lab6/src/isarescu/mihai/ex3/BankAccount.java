package isarescu.mihai.ex3;

import java.util.Objects;

public class BankAccount implements Comparable<BankAccount> {
    private String owner;
    private double balance;


    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public void withdraw(double amount){
        balance -= amount;
    }

    public void deposit(double amount){
        balance += amount;
    }

    public String getOwner(){
        return owner;
    }

    //equals suprascris sa compare doar owner-ul
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankAccount that = (BankAccount) o;
        return owner.equals(that.owner);
    }

    public double getBalance() {
        return balance;
    }

    //hashCode suptrascris pentru owner si pentru balance
    @Override
    public int hashCode() {
        return Objects.hash(owner, balance);
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "owner='" + owner + '\'' +
                ", balance=" + balance +
                '}';
    }

    @Override
    public int compareTo(BankAccount o) {
        return (int)(balance-o.balance);
    }
}
