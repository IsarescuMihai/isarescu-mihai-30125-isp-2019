package isarescu.mihai.ex3;

import java.util.TreeSet;
import java.util.SortedSet;
import java.util.Collections;

public class Bank {

    private TreeSet<BankAccount> accounts;

    public Bank(TreeSet<BankAccount> accounts) {
        this.accounts = accounts;
    }

    public void addAccount(String owner, double balance){
        accounts.add(new BankAccount(owner,balance));
    }

    public void printAccounts(){
        for(BankAccount acc : accounts){
            System.out.println(acc.toString());
        }
    }

    public TreeSet<BankAccount> getAllAccounts(){
        return accounts;
    }

    public void printAccounts(double minBalance, double maxBalance){
        SortedSet<BankAccount> goodAccounts = accounts.subSet(new BankAccount("ba1",minBalance), new BankAccount("ba2",maxBalance));
        for(BankAccount acc : goodAccounts){
            System.out.println(acc.toString());
        }
    }

    public BankAccount getAccount(String owner){
        BankAccount ba = new BankAccount(owner,0);
        BankAccount Return = null;
        for(BankAccount acc : accounts){
            if(acc.equals(ba))
                Return = acc;
        }
        return Return;
    }
}
