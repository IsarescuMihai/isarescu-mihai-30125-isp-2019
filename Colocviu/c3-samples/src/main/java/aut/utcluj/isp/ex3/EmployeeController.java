package aut.utcluj.isp.ex3;

import java.util.ArrayList;
import java.util.List;

/**
 * @author stefan
 */
public class EmployeeController {
    /**
     * Add new employee to the list of employees
     *
     * @param employee - employee information
     */
    List<Employee> employees = new ArrayList<>();

    public void addEmployee(final Employee employee) {
        employees.add(employee);
    }

    /**
     * Get employee by cnp
     *
     * @param cnp - unique cnp
     * @return found employee or null if not found
     */
    public Employee getEmployeeByCnp(final String cnp) {
        Employee a = new Employee("","",0d,cnp);
        if(employees.contains(a)){
            int index = employees.indexOf(a);
            return employees.get(index);
        }
        else return null;
    }

    /**
     * Update employee salary by cnp
     *
     * @param cnp    - unique cnp
     * @param salary - salary
     * @return updated employee
     */
    public Employee updateEmployeeSalaryByCnp(final String cnp, final Double salary) {
        Employee a = new Employee("","",0d,cnp);
        if(employees.contains(a)){
            int index = employees.indexOf(a);
            a.setFirstName(employees.get(index).getFirstName());
            a.setLastName(employees.get(index).getLastName());
            a.setCnp(employees.get(index).getCnp());
            a.setSalary(salary);
            employees.set(index,a);
            return a;
        }
        return null;
    }

    /**
     * Delete employee by cnp
     *
     * @param cnp - unique cnp
     * @return deleted employee or null if not found
     */
    public Employee deleteEmployeeByCnp(final String cnp) {
        Employee a = new Employee("","",0d,cnp);
        if(employees.contains(a)){
            int index = employees.indexOf(a);
            return employees.remove(index);
        }
        else return null;
    }

    /**
     * Return current list of employees
     *
     * @return current list of employees
     */
    public List<Employee> getEmployees() {
        return employees;
    }

    /**
     * Get number of employees
     *
     * @return - number of registered employees
     */
    public int getNumberOfEmployees() {
        return employees.size();
    }
}
