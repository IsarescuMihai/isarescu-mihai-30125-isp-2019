package isarescu.mihai.lab4.ex1;
//import java.util.Scanner;
//import isarescu.mihai.lab4.ex1.Circle;

public class TestCircle {
    public static void main(String[] args) {
        Circle c1 = new Circle();
        Circle c2 = new Circle(4.0f);
        System.out.println("Circle one has " + c1.getRadius() + " radius and " + c1.getArea() + " area");
        System.out.println("Circle two has " + c2.getRadius() + " radius and " + c2.getArea() + " area");
    }
}
