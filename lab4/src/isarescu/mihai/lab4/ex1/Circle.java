package isarescu.mihai.lab4.ex1;

public class Circle {
    private double radius = 1.0f;
    private String color = "red";

    public Circle(){}
    public Circle(double radius){
        this.radius = radius;
    }

    public double getRadius() {
        return this.radius;
    }

    public double getArea(){
        return 2*3.14*this.radius*this.radius;
    }
}
