package isarescu.mihai.lab4.ex3;
import isarescu.mihai.lab4.ex2.Author;

public class TestBook {
    public static void main(String[] args) {
        Author aut = new Author("Ion Creanga","Ion.Creanga@humulesti.ro",'m');
        Book b1 = new Book("Amintiri din copilarie",aut,10.50);
        Book b2 = new Book("Harap Alb",null,11.50,10);

        System.out.println(b1.toString());
        System.out.println(b2.toString());
    }
}
