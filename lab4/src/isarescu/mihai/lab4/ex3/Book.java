package isarescu.mihai.lab4.ex3;
import isarescu.mihai.lab4.ex2.Author;

public class Book {
    private String name;
    private Author author;
    private double price;
    private int quantityInStock = 0;//este mai clar decat qtyInStock

    public Book(String name, Author author, double price){
        this.name = name;
        //in caz ca primim un autor neinstantiat
        if(author == null)
            this.author = new Author("no one","none",'n');
        else
            this.author = author;
        this.price = price;
    }
    public Book(String name, Author author, double price, int quantityInStock){
        this.name = name;
        //in caz ca primim un autor neinstantiat
        if(author == null)
            this.author = new Author("no one","none",'n');
        else
            this.author = author;
        this.price = price;
        this.quantityInStock = quantityInStock;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQuantityInStock(int quantityInStock) {
        this.quantityInStock = quantityInStock;
    }

    public String getName() {
        return name;
    }

    public Author getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public int getQuantityInStock() {
        return quantityInStock;
    }

    @Override
    public String toString() {
        return name + " by " + author.toString();
    }
}
