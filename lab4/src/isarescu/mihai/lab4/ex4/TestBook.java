package isarescu.mihai.lab4.ex4;
import isarescu.mihai.lab4.ex2.Author;
public class TestBook {
    public static void main(String[] args) {
        Author[] listOfAuthors = new Author[3];
        listOfAuthors[0] = new Author("Ion Creanga","Editura@iasi.ro",'m');
        listOfAuthors[1] = new Author("Mihai Eminescu", "Mihai@Eminescu.ro", 'm');
        listOfAuthors[2] = new Author("Ioan Slavici","Ioan.Slavici@carte.ro", 'm');

        Book b1 = new Book("Cartea 1",listOfAuthors,49.99);

        System.out.println(b1.toString());
    }
}
