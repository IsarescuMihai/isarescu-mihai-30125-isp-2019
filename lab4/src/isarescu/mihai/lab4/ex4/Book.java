package isarescu.mihai.lab4.ex4;
import isarescu.mihai.lab4.ex2.Author;

public class Book {
    private String name;
    private Author[] authors;
    private double price;
    private int quantityInStock = 0;//este mai clar decat qtyInStock

    public Book(String name, Author[] authors, double price){
        this.name = name;
        //in caz ca primim un autor neinstantiat
        if(authors == null) {
            this.authors = new Author[1];
            this.authors[0] = new Author("no one","none",'n');
        }
        else {
            this.authors = authors;
        }
        this.price = price;
    }
    public Book(String name, Author[] authors, double price, int quantityInStock){
        this.name = name;
        //in caz ca primim un autor neinstantiat
        if(authors == null){
            this.authors = new Author[1];
            this.authors[0] = new Author("no one","none",'n');
        }
        else{
            this.authors = authors;
        }

        this.price = price;
        this.quantityInStock = quantityInStock;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQuantityInStock(int quantityInStock) {
        this.quantityInStock = quantityInStock;
    }

    public String getName() {
        return name;
    }

    public Author[] getAuthors() {
        return authors;
    }

    public double getPrice() {
        return price;
    }

    public int getQuantityInStock() {
        return quantityInStock;
    }

    @Override
    public String toString() {
        return name + " by " + authors.length + " authors";
    }
}
