package isarescu.mihai.lab4.ex6;

public class Shape {
    private String color = "red";
    private boolean filled = true;

    public Shape(){ }
    public Shape(String color, boolean filled){
        this.color = color;
        this. filled = filled;
    }

    public String getColor() {
        return color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    @Override
    public String toString() {
        if(this.filled == true)
            return "A shape with color of " + this.color + " and filled";
        else
            return "A shape with color of " + this.color + " and Not filled";
    }
}
