package isarescu.mihai.lab4.ex6;

public class Square extends Rectangle{
    public Square(){
        super();
    }
    public Square(double side){
        super(side,side);
    }
    public Square(double side, String color, boolean filled){
        super(side,side,color,filled);
    }

    @Override
    public void setLength(double side){
        setSide(side);
    }
    @Override
    public void setWidth(double side){
        setSide(side);
    }

    public double getSide(){ return super.getLength(); }
    public void setSide(double side){
        super.setLength(side);
        super.setWidth(side);
    }

    @Override
    public String toString() {
        return "A Square which is a subclass of " + super.toString();
    }
}
