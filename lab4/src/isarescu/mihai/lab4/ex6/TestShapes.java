package isarescu.mihai.lab4.ex6;

public class TestShapes {
    public static void main(String[] args) {
        Shape shape1 = new Shape();
        Shape shape2 = new Shape("blue",false);

        Circle circle1 = new Circle();
        Circle circle2 = new Circle(2.0f);
        Circle circle3 = new Circle(3.0f,"Green",false);

        Rectangle rect1 = new Rectangle();
        Rectangle rect2 = new Rectangle(2,3);
        Rectangle rect3 = new Rectangle(3,4,"yellow",false);

        Square square1 = new Square();
        Square square2 = new Square(10.0f);
        Square square3 = new Square(5.0f,"cyan",true);

        System.out.println("shape1");
        System.out.println(shape1.toString());
        System.out.println("shape2");
        System.out.println(shape2.toString());
        System.out.println();

        System.out.println("circle1");
        System.out.println(circle1.toString());
        System.out.println("circle2");
        System.out.println(circle2.toString());
        System.out.println("circle3");
        System.out.println(circle3.toString());
        System.out.println();

        System.out.println("rectangle1");
        System.out.println(rect1.toString());
        System.out.println("rectangle2");
        System.out.println(rect2.toString());
        System.out.println("rectangle3");
        System.out.println(rect3.toString());
        System.out.println();

        System.out.println("square1");
        System.out.println(square1.toString());
        System.out.println("square2");
        System.out.println(square2.toString());
        System.out.println("square3");
        System.out.println(square3.toString());
    }
}
