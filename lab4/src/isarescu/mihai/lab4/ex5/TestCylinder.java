package isarescu.mihai.lab4.ex5;

public class TestCylinder {
    public static void main(String[] args) {
        Cylinder c1 = new Cylinder();
        Cylinder c2 = new Cylinder(1.0);
        Cylinder c3 = new Cylinder(1.0,2.0);

        System.out.println(c1.getVolume());
        System.out.println(c2.getVolume());
        System.out.println(c3.getVolume());
    }
}
