package isarescu.mihai.lab4.ex5;
import isarescu.mihai.lab4.ex1.Circle;

public class Cylinder extends Circle{
    private double height = 1;
    public Cylinder(){
        super();
    }
    public Cylinder(double radius){
        super(radius);
    }
    public Cylinder(double radius, double height){
        super(radius);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public double getVolume(){
        //getArea mostenit din clasa Circle
        return getArea()*this.height;
    }
}
