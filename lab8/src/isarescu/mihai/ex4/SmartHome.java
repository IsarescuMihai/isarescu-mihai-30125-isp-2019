package isarescu.mihai.ex4;

public class SmartHome extends Home {


    @Override
    protected void setValueInEnvironment(Event event) {
        System.out.println("the event is: " + event.getType());
    }

    @Override
    protected void controllStep() {
        System.out.println("controll step is activated");
    }
}
