package isarescu.mihai.ex4;

import javax.swing.*;
import java.awt.*;

public class Game extends JFrame{
    int width = 600;
    int height = 600;
    String gameState = "x";
    static String endGameMessage = "Game Over ";

    JTextArea[][] tiles;
    static JButton[][] buttons;
    JLabel gameStateText;
    static int filledButtons = 9;

    public Game(){
        setTitle("X si 0");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(width,height);
        setVisible(true);
        setLayout(null);

        tiles = new JTextArea[3][3];
        buttons = new JButton[3][3];
        gameStateText = new JLabel();

        gameStateText.setText("x pune primul");
        gameStateText.setBounds((int)(0.4*height),0,(int)(0.2*width),(int)(0.1*height));


        //draw the buttons
        String buttonText = " ";
        for(int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                buttons[i][j] = new JButton();
                buttons[i][j].setBounds((int)((0.1+i*0.25)*width),(int)((0.1+j*0.25)*height),(int)((0.25)*width),(int)((0.25)*height));
                buttons[i][j].setText("");
                buttons[i][j].addActionListener((e)->{
                    JButton b = (JButton)e.getSource();
                    if(!b.getText().equals("x") && !b.getText().equals("0")) {
                        b.setText(gameState);
                        b.setFont(new Font("courier new", Font.BOLD, 50));
                        filledButtons--;
                        if (gameState.equals("x"))
                            gameState = "0";
                        else
                            gameState = "x";
                        gameStateText.setText(gameState + " pune urmatorul");
                    }
                });
                add(buttons[i][j]);
            }
        }

        System.out.println("maria"+buttons[0][0].getText());

        add(gameStateText);
        repaint();
    }

    //check if game is done
    static boolean gameShouldExit(){
        if(filledButtons == 0)
        {
            endGameMessage = endGameMessage + " DRAW!";
            return true;
        }
        String a0 ="";
        for(int i = 0; i < 3; i++){
            a0 = a0 + buttons[0][i].getText();
        }
        if(a0.equals("xxx") || a0.equals("000")){
            endGameMessage = endGameMessage + a0.charAt(0) + " WON!";
            return true;
        }

        a0 ="";
        for(int i = 0; i < 3; i++){
            a0 = a0 + buttons[1][i].getText();
        }
        if(a0.equals("xxx") || a0.equals("000")){
            endGameMessage = endGameMessage + a0.charAt(0) + " WON!";
            return true;
        }

        a0 ="";
        for(int i = 0; i < 3; i++){
            a0 = a0 + buttons[2][i].getText();
        }
        if(a0.equals("xxx") || a0.equals("000")){
            endGameMessage = endGameMessage + a0.charAt(0) + " WON!";
            return true;
        }

        a0 = "";
        for(int i = 0; i < 3; i++){
            a0 = a0 + buttons[i][0].getText();
        }
        if(a0.equals("xxx") || a0.equals("000")){
            endGameMessage = endGameMessage + a0.charAt(0) + " WON!";
            return true;
        }

        a0 = "";
        for(int i = 0; i < 3; i++){
            a0 = a0 + buttons[i][1].getText();
        }
        if(a0.equals("xxx") || a0.equals("000")){
            endGameMessage = endGameMessage + a0.charAt(0) + " WON!";
            return true;
        }

        a0 = "";
        for(int i = 0; i < 3; i++){
            a0 = a0 + buttons[i][2].getText();
        }
        if(a0.equals("xxx") || a0.equals("000")){
            endGameMessage = endGameMessage + a0.charAt(0) + " WON!";
            return true;
        }

        a0 = "";
        for(int i = 0; i < 3; i++){
            a0 = a0 + buttons[i][i].getText();
        }
        if(a0.equals("xxx") || a0.equals("000")){
            endGameMessage = endGameMessage + a0.charAt(0) + " WON!";
            return true;
        }

        a0 = "";
        for(int i = 0; i < 3; i++){
            a0 = a0 + buttons[2-i][i].getText();
        }
        if(a0.equals("xxx") || a0.equals("000")){
            endGameMessage = endGameMessage + a0.charAt(0) + " WON!";
            return true;
        }


        return false;
    }

    public static void main(String[] args) {
        Game g = new Game();

        while(!gameShouldExit()){

        }

        JOptionPane.showMessageDialog(null, endGameMessage);
    }
}
