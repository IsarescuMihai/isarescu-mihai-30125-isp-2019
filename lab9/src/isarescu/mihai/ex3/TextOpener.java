package isarescu.mihai.ex3;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.*;

public class TextOpener extends JFrame {

    String fileContent;
    JTextArea text;
    JTextField fileName;
    JButton openButton;
    int width = 500;
    int height = 600;



    public TextOpener(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Text Opener");
        setVisible(true);
        setLayout(null);
        this.getContentPane().setBackground(Color.cyan);
        setSize(width,height);

        openButton = new JButton();
        openButton.setBounds((int)(0.7*width),(int)(0.05*height),(int)(0.15*width),(int)(0.1*height));
        openButton.setFont(new Font("courier new", Font.PLAIN, 14));
        openButton.setText("open");
        openButton.setBackground(Color.yellow);
        openButton.setForeground(Color.red);
        openButton.addMouseListener(new MouseListener(){
            @Override
            public void mouseClicked(MouseEvent e) {
                File file = new File("D:\\Work\\Important work\\POLI STUFF\\ISP\\isarescu-mihai-30125-isp-2019\\lab9\\src\\isarescu\\mihai\\ex3\\" + fileName.getText());
                try {
                    BufferedReader input = new BufferedReader(new FileReader(file));
                    String st;
                    fileContent = "";
                    while ((st = input.readLine()) != null)
                        fileContent = fileContent + st + "\r\n";
                    //System.out.println(fileContent);
                    text.append(fileContent);

                } catch (IOException e1) {
                   // e1.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Cannot find this file!");
                }
            }
            @Override
            public void mousePressed(MouseEvent e) { }
            @Override
            public void mouseReleased(MouseEvent e) { }
            @Override
            public void mouseEntered(MouseEvent e) { }
            @Override
            public void mouseExited(MouseEvent e) { }
        });

        text = new JTextArea();
        fileName = new JTextField();

        fileName.setFont(new Font("courier new", Font.BOLD, 20));
        fileName.setBounds((int)(0.1*width),(int)(0.05*height),(int)(0.60*width),(int)(0.1*height));

        text.setFont(new Font("courier new", Font.PLAIN, 14));
        text.setLineWrap(true);
        text.setBounds((int)(0.1*width),(int)(0.16*height),(int)(0.75*width),(int)(0.7*height));
        text.setText("Default text");

        add(openButton);
        add(text);
        add(fileName);
        repaint();
    }

    public static void main(String[] args) {
        TextOpener to = new TextOpener();
    }
}
