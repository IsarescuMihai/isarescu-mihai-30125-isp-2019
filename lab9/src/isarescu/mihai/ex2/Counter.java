package isarescu.mihai.ex2;

import javax.swing.*;
import java.awt.*;
//import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;


public class Counter extends JFrame {

    int width = 400, height = 400;
    int count = 0;

    JButton buton = new JButton();
    JTextField text = new JTextField();

    public Counter(){
        setTitle("counter apasari buton");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        this.setLayout(null);
        this.getContentPane().setBackground(Color.YELLOW);
        setSize(width,height);

        buton.setBounds((int)(0.1*width),(int)(0.1*height),(int)(0.75*width),(int)(0.2*height));
        buton.setText("apasa-ma");
        buton.setForeground(Color.CYAN);
        buton.setBackground(Color.red);

        buton.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                count++;
                text.setText(Integer.toString(count));
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        text.setBounds((int)(0.1*width),(int)(0.35*height),(int)(0.75*width),(int)(0.4*height));
        text.setText(Integer.toString(count));
        text.setFont(new Font("courier new", Font.BOLD, 20));

        add(buton);
        add(text);
        this.repaint();

    }



    public static void main(String[] args) {
        Counter c = new Counter();
    }
}
